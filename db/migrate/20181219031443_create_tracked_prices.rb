class CreateTrackedPrices < ActiveRecord::Migration[5.2]
  def change
    create_table :tracked_prices do |t|
      t.decimal :buy_price
      t.decimal :sell_price
      t.integer :tracked_item_id

      t.timestamps
    end
  end
end
