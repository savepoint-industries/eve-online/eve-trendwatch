class CreateTrackedItems < ActiveRecord::Migration[5.2]
  def change
    create_table :tracked_items, id: false do |t|
      t.integer :item_id, null: false
      t.string :item_friendlyname
      t.index :item_id, unique: true

      t.timestamps
    end
  end
end
