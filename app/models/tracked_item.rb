class TrackedItem < ApplicationRecord
	self.primary_key = 'item_id'
	has_many :tracked_prices, dependent: :destroy
end
