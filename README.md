# README

Versions

* 0.0.X - Usually bug fixes, and background work.

* 0.X.0 - Small scale features, minor and major bug fixes.

* X.0.0 - Large scale, program changing features. Will usually be followed by a 0.0.X release to fix bugs.

Using Trend-Watch

* Input item name

* Input quantity

* Input Market location (Jita, Amarr, etc)

* Output will be Buy prices, Sell prices, 24 hour, 7 day, and 30 day trends.

Site will be able to be used without a log in. A profile will allow you to set favorite market locations, get a quick overview of your character profile via API intergration, as well as other, future features that require a profile. 
